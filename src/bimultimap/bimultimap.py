from collections import defaultdict
from collections.abc import Hashable, ItemsView, Iterable, Iterator, KeysView, Mapping, MutableMapping, ValuesView
from typing import Generic, TypeVar, cast

K = TypeVar("K", bound=Hashable)
V = TypeVar("V", bound=Hashable)


class BiMultiMap(MutableMapping, Generic[K, V]):
    _forward: defaultdict[K, list[V]]
    _inverse: dict[V, K]

    def __init__(self) -> None:
        self._forward = defaultdict(list)
        self._inverse = {}

    # Collection
    def __contains__(self, key: K) -> bool:
        return key in self._forward

    def __iter__(self) -> Iterator[K]:
        return iter(self._forward)

    def __len__(self) -> int:
        return len(self._forward)

    # Mapping
    def __getitem__(self, key: K) -> list[V]:
        return self._forward[key]

    def keys(self) -> KeysView[K]:
        return self._forward.keys()

    def items(self) -> ItemsView[K, list[V]]:
        return self._forward.items()

    def values(self) -> ValuesView[V]:
        return cast(ValuesView[V], self._inverse.keys())

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Mapping):
            return self._forward.items() == other.items()

        return NotImplemented

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    # MutableMapping
    def __setitem__(self, key: K, value: V) -> None:
        self._forward[key].append(value)
        self._inverse[value] = key

    def __delitem__(self, key: K) -> None:
        for value in self._forward[key]:
            del self._inverse[value]
        del self._forward[key]

    def pop(self, key: K) -> list[V]:
        vals = self._forward.pop(key)
        for value in vals:
            del self._inverse[value]
        return vals

    def popitem(self) -> tuple[K, list[V]]:
        key, vals = self._forward.popitem()
        for value in vals:
            del self._inverse[value]
        return key, vals

    def clear(self) -> None:
        self._forward.clear()
        self._inverse.clear()

    # TODO: update and setdefault
    # def setdefault(self) -> None:
    #     ...
    # def update(self) -> None:
    #     ...

    # Mostly follow basic bidict
    # Extra reverse methods from bidict
    def __inverted__(self) -> Iterator[tuple[V, K]]:
        return iter(self._inverse.items())

    # Own API
    def get_inverse(self, key: V) -> K:
        return self._inverse[key]

    def keys_inverse(self) -> KeysView[V]:
        return self._inverse.keys()

    def items_inverse(self) -> ItemsView[V, K]:
        return self._inverse.items()

    def values_inverse(self) -> ValuesView[K]:
        return cast(ValuesView[K], self._inverse.values())

    def set_iterable(self, key: K, values: Iterable[V]) -> None:
        self._forward[key] = list(values)
        for value in values:
            self._inverse[value] = key

    # Utility
    def __repr__(self) -> str:
        clsname = self.__class__.__name__
        return f"<{clsname} id={id(self)}>"

    def __str__(self) -> str:
        return str(self._forward)
